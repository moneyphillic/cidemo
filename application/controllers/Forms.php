<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends CI_Controller {

	public function index () {

		$this->load->library('form_validation');
		if(isset($_POST['send']))
		{
			$this->load->model('form_rules_model');
			$this->form_validation->set_rules($this->form_rules_model->add_rules);

			$check = $this->form_validation->run();

			if ($check == TRUE)
			{
				$add['names'] = $this->input->post('names');
				$add['email'] = $this->input->post('email');
				$add['message'] = $this->input->post('message');
				$add['date'] = date('Y-m-d');
				$this->db->insert('users', $add);
				#$this->load->view('info_view');
			}
			else
			{
				echo "heeey";
			}
		}

		else 
		{
			$this->load->view('forms_view');
		}
	}

}
