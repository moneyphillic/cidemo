<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_rules_model extends CI_Model {

	public $add_rules = array(

		array(
			'field' = 'names';
			'label' = 'Enter your names';
			'rules' = 'required|min_length[5]|max_length[40]|alpha'
		),
		array(
			'field' = 'email';
			'label' = 'Enter your email';
			'rules' = 'required|valid_email'
		)
		array(
			'field' = 'message';
			'label' = 'Leave your message';
			'rules' = 'required|min_length[80]'
		)

	);

}