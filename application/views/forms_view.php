<!DOCTYPE html>
<html>
<head>
	<title>Forms View</title>
	<style>
		label {
			display: block;
			text-align: center;
			padding: 10px;
		}
		input, textarea {
			width: 30%;
			margin-left: 35%;
			margin-top: 0px;
			padding-top: 0px;
		}
	</style>
</head>
<body>

	<form method="post" action="http://localhost/cidemo/forms">
		<label>Enter your names</label><br>
		<input type="text" name="names"><?=form_error('names')?><br>
		<label>Enter your email</label><br>
		<input type="email" name="email"><?=form_error('email')?><br>
		<label>Leave your message</label><br>
		<textarea name="message" rows="10" cols="30"></textarea><?=form_error('message')?><br>
		<input type="submit" name="send" value="SEND">
	</form>

</body>
</html>